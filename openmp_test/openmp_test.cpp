// openmp_test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <random>
#include "omp.h"
#include <chrono>
#include <ratio>
#include <iostream>
#define POC 100000000
using namespace std::chrono;

int main()
{
	double suma = 0;
	std::vector<int> cisla;
	cisla.resize(POC);
#pragma omp parallel
	{
		
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> cis(0, 500000000);
#pragma omp for
		for (int i = 0; i < POC; i++)
		{
			cisla[i]=(cis(gen));
		}
	}
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	double minmin = 9999999;
	for (int i = 0; i < POC; i++)
	{
		if (pow(cisla[i],0.256) < minmin)minmin = pow(cisla[i],0.256);
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

	std::cout << "It took me " << time_span.count() << " seconds.\n";
	printf("globalne minimum je %lf\n", minmin);

	t1 = high_resolution_clock::now();
	double glob_min = 999999;
#pragma omp parallel
	{
		double lok_min=999999;
#pragma omp for
		for (int i = 0; i < POC; i++)
		{
			if (pow(cisla[i], 0.256) < lok_min)lok_min = pow(cisla[i], 0.256);
		}
		printf("vlakno %d moje lokalne minimum je %lf\n", omp_get_thread_num(), lok_min);
#pragma omp critical
		{
			if (glob_min > lok_min)glob_min = lok_min;
		}
	}
	t2 = high_resolution_clock::now();
	time_span = duration_cast<duration<double>>(t2 - t1);

	std::cout << "It took me " << time_span.count() << " seconds.\n";
	printf("globalne minimum je %lf\n", glob_min);

	t1 = high_resolution_clock::now();
	std::vector<int> parne;
	for (int i = 0; i < POC; i++)
	{
		if (cisla[i] % 2 == 0)parne.push_back(cisla[i]);
	}
	t2 = high_resolution_clock::now();
	time_span = duration_cast<duration<double>>(t2 - t1);

	std::cout << "It took me " << time_span.count() << " seconds.\n";
	printf("celkovy pocet parnych je %zd\n", parne.size());

	t1 = high_resolution_clock::now();
	std::vector<int> vysledok;
#pragma omp parallel
	{
		std::vector<int> lokalpar;
#pragma omp for
		for (int i = 0; i < POC; i++)
		{
			if (cisla[i] % 2 == 0)lokalpar.push_back(cisla[i]);
		}
		//printf("vlakno %d moje lokalne minimum je %lf\n", omp_get_thread_num(), lok_min);
/*#pragma omp critical
		{
			for (int i = 0; i < lokalpar.size(); i++)
			{
				vysledok.push_back(lokalpar[i]);
			}
		}*/
#pragma omp for ordered schedule(static,1)
		for (int t = 0; t < omp_get_num_threads(); t++)
		{
#pragma omp ordered
			{
				for (int i = 0; i < lokalpar.size(); i++)
				{
					vysledok.push_back(lokalpar[i]);
				}
			}
		}
	}
	t2 = high_resolution_clock::now();
	time_span = duration_cast<duration<double>>(t2 - t1);

	std::cout << "It took me " << time_span.count() << " seconds.\n";
	printf("celkovy pocet parnych je %zd\n", vysledok.size());
	for (int i = 0; i < parne.size(); i++)
	{
		if (i % (parne.size() / 20) == 0)printf("%d %d\n", parne[i], vysledok[i]);
	}
    return 0;
}

